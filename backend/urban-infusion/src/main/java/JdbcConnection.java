import java.sql.*;

public class JdbcConnection {

    // Established connection
    private Connection connection;
    // Singleton instance of the class
    private static JdbcConnection instance;

    /**
     * Constructor is private in order to prevent creation of instances directly, getInstance() is used instead.
     */
    private JdbcConnection() {

    }

    /**
     * Get an instance of the connection class.
     * 
     * @return Singleton instance of connection class
     */
    public static JdbcConnection getInstance() {
        // Initialization of singleton instance
        if (instance == null) {
            instance = new JdbcConnection();
        }
        return instance;
    }

    /**
     * Connect to the database.
     * 
     * @param host          Host of the database (localhost, IP address or host name)
     * @param port          TCP port number (3306 by default)
     * @param database      Database name
     * @param user          SQL user
     * @param password      SQL user password
     * @throws Exception    Throws an exception if the connection was not successful
     */
    public void connect(String host, int port, String database, String user, String password) throws Exception {
        this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/"
                            + database + "?user=" + user + "&password=" + password);
    }


    /**
     * Check if connection to database is established
     * 
     * @return True if connection established, false if not
     */
    public boolean isConnected() {
        return this.connection != null;
    }


}