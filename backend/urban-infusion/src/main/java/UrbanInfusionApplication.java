
public class UrbanInfusionApplication {
    public static void main(String[] args) {
        JdbcConnection connection = JdbcConnection.getInstance();
        try {
            connection.connect("localhost", 3306, "test", "admin", "admin");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String status = connection.isConnected() ? "established" : "failed";
        System.out.println("Connection " + status);
    }
}
